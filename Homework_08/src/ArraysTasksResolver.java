public class ArraysTasksResolver {
     static void resolveTask(int[] array, ArrayTask task, int from, int to) {
        System.out.println("Диапазон от " + from + " до " + to);
        for(int i = from; i < to; i++ ) {
            System.out.println(array[i] + "  ");
        }
        System.out.println("Результат " + task.resolve(array,from,to));

    }
}
