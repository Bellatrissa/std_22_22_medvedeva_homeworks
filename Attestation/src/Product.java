public class Product {
    private Integer number;
    private String nameProduct;
    private Double price;
    private Integer amountProduct;
    public Product(Integer number,String nameProduct,Double price,Integer amountProduct) {
        this.number = number;
        this.nameProduct = nameProduct;
        this.price = price;
        this.amountProduct = amountProduct;
    }
    public Integer getNumber() {
        return number;
    }
    public void setNumber(Integer number) {
        this.number =number;
    }
    public String getNameProduct() {
        return nameProduct;
    }
    public void setNameProduct(String nameProduct){
        this.nameProduct =nameProduct;
    }
    public Double getPrice() {
        return price;
    }
    public void setPrice(Double price) {
        this.price = price;
    }
    public Integer getAmountProduct() {
        return amountProduct;
    }
    public void setAmountProduct(Integer amountProduct){
        this.amountProduct = amountProduct;
    }
    public String toString() {
        return "Product{" +
                "number='" + number + '\'' +
                "nameProduct='" + nameProduct + '\'' +
                "price'" + price + '\'' +
                "amountProduct'" + amountProduct +
                '}';
    }
}
