import java.io.IOException;
public class Main {
    public static void main(String[] args) {
        ProductsRepository productsRepository = new ProductsRepositoryFileBasedImp("products");
        System.out.println(productsRepository.findById(2));
        System.out.println(productsRepository.findAllByTitleLike("ко"));
        Product milk = productsRepository.findById(1);
        milk.setPrice(50.8);
        milk.setAmountProduct(20);
        productsRepository.update(milk);
        System.out.println(productsRepository.findById(1));
    }
}