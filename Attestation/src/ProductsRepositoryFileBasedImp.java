import java.util.List;
import java.io.*;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.io.IOException;
public class ProductsRepositoryFileBasedImp implements ProductsRepository{
        private final String fileName;

        public ProductsRepositoryFileBasedImp(String fileName) {
            this.fileName = fileName;
        }

        private static final Function<String, Product> stringToProductMapper = currentProduct -> {
            String[] parts = currentProduct.split("\\|");
            Integer number = Integer.parseInt (parts[0]);
            String nameProduct = parts[1];
            Double price = Double.parseDouble (parts[2]);
            Integer amountProduct = Integer.parseInt(parts[3]);
            return new Product(number, nameProduct, price, amountProduct);
        };
    private static final Function<Product, String> productToStringMapper = product ->
            product.getNumber() + "|" + product.getNameProduct() + "|" + product.getPrice() +
                    "|" + product.getAmountProduct();


    @Override
    public Product findById(Integer id) {
            try {
                return new BufferedReader(new FileReader(fileName))
                        .lines()
                        .map(stringToProductMapper)
                        .filter(product -> product.getNumber().equals (id))
                        .findFirst().get();

            } catch (IOException e) {
                throw new UnsuccessfulWorkWithFileException(e);
            }
        }

    @Override
    public List<Product> findAllByTitleLike(String title) {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToProductMapper)
                    .filter(product -> product.getNameProduct().toLowerCase().contains(title.toLowerCase()))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }

    @Override
    public void update(Product product)  {
        try  (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            List<Product> productList = reader.lines().map(stringToProductMapper).toList();
            Product oldProduct = productList.stream().filter(it -> it.getNumber().equals(product.getNumber()))
                    .findFirst().get();
            Product newProduct = new Product(oldProduct.getNumber(), product.getNameProduct(),
                    product.getPrice(), product.getAmountProduct());
            List<Product> products = productList.stream().map(it -> {
                if (Objects.equals(it.getNumber(), newProduct.getNumber())) {
                    return newProduct;
                }
                return it;
            }).toList();
            saveALL(products);
        } catch(IOException e){
                throw new UnsuccessfulWorkWithFileException(e);
            }
    }
    public void saveALL(List<Product>products) throws UnsuccessfulWorkWithFileException {
        try(FileWriter fileWriter = new FileWriter(fileName, false);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter))
        {
            StringBuilder stringsProduct = new StringBuilder();
            for(Product product : products) {
                stringsProduct.append(productToStringMapper.apply(product)).append("\n");
            }
            bufferedWriter.write(stringsProduct.toString());
        }catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }
}
