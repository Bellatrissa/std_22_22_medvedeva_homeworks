public class Bankomat {
    private int balance;
    private final int maxSum;
    private final int maxBalance;
    private int sumOperations;
    Bankomat(int maxSum, int maxBalance, int balance) {
        this.balance = balance;
        this.maxSum = maxSum;
        this.maxBalance = maxBalance;
        this.sumOperations = sumOperations;
    }
    public int withdrawal(int sumWithdrawal) {
        this.sumOperations ++;
        if(sumWithdrawal <= balance && sumWithdrawal <= maxSum) {
            this.balance = balance - sumWithdrawal;
            return sumWithdrawal;
        }
        return 0;
    }
    public int depositing(int sumDepositing) {
        this.sumOperations ++;
        if(sumDepositing + balance > maxBalance) {
            int oldBalance = balance;
            balance = maxBalance;
            return sumDepositing + oldBalance - maxBalance;
        }
        return 0;
    }
    public int getBalance() {
        return balance;
    }
    public int getSumOperations() {
        return sumOperations;
    }
}
