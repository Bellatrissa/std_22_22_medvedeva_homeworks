create table workers(
    id bigserial primary key,
    first_name char(20),
    last_name char(20),
    number_phone bigint unique,
    experience integer,
    age  integer,
    certificate bool,
    category_certificate char(20),
    rating integer check (rating >= 0 and rating <= 5)
);

create table car(
    id bigserial primary key ,
    model char(50),
    color char(20),
    number char(10),
    id_owner int not null,
    foreign key (id_owner) references workers(id)
);
create table trip(
    id bigserial primary key,
    id_driver int not null,
    foreign key (id_driver) references workers(id),
    id_car int not null,
    foreign key (id_car) references car(id),
    data_trip timestamp,
    trip_duration time
);