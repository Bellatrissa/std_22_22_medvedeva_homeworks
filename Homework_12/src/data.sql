
insert into workers (first_name, last_name, number_phone, experience, age, certificate, category_certificate, rating)
values ('Евгений','Медведев',89023514592,10,31,true,'B',5);
insert into workers (first_name, last_name, number_phone, experience, age, certificate, category_certificate, rating)
values ('Юлия','Медведева',89023514678,1,31,true,'B',2);
insert into workers (first_name, last_name, number_phone, experience, age, certificate, category_certificate, rating)
values ('Пётр','Сидоров',89172314592,5,25,true,'B',5);
insert into workers (first_name, last_name, number_phone, experience, age, certificate, category_certificate, rating)
values ('Андрей','Болконский',89273514575,15,38,true,'B',4);
insert into workers (first_name, last_name, number_phone, experience, age, certificate, category_certificate, rating)
values ('Рейнира','Таргариен',89053614591,2,20,true,'B',5);

insert into car (model, color, number, id_owner)
values ('Camry','red','A777AA',1),
       ('Colt','black','A700AA',2),
       ('Logan','white','A300AA',3),
       ('Sonata','black','B666OP',4),
       ('Prius','Green','O600AA',5);

insert into trip (id_driver, id_car, data_trip, trip_duration)
values (1,1,'2022-11-08','1:10'),
       (2,2,'2022-11-03','0:39'),
       (3,3,'2022-10-30','1:20'),
       (4,4,'2022-10-29','0:45'),
       (5,5,'2022-10-08','1:05');

