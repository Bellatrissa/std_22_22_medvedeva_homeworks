package ru.inno.webapp.ec.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.webapp.ec.dto.LessonForm;
import ru.inno.webapp.ec.model.Lesson;
import ru.inno.webapp.ec.repositories.LessonsRepository;
import ru.inno.webapp.ec.service.LessonsService;

import java.util.List;
@RequiredArgsConstructor
@Service

public class LessonsServiceImpl implements LessonsService {
    private final LessonsRepository lessonsRepository;

    @Override
    public List<Lesson> getAllLessons() {
        return lessonsRepository.findAllByStatusNot(Lesson.Status.DELETED);
    }

    @Override
    public Lesson getLesson(Long lessonId) {
        return lessonsRepository.findById(lessonId).orElseThrow();
    }

    @Override
    public void deleteLesson(Long lessonId) {
        Lesson lessonForDelete = lessonsRepository.findById(lessonId).orElseThrow();
        lessonForDelete.setStatus(Lesson.Status.DELETED);
        lessonsRepository.save(lessonForDelete);
    }

    @Override
    public void addLesson(LessonForm lesson) {
        Lesson newLesson = Lesson.builder()
                .name(lesson.getName())
                .summary(lesson.getSummary())
                .startTime(lesson.getStartTime())
                .finishTime(lesson.getFinishTime())
                .status(Lesson.Status.NOT_CONFIRMED)
                .build();
        lessonsRepository.save(newLesson);
    }

    @Override
    public void updateLesson(Long lessonId, LessonForm updateDate) {
        Lesson lessonForUpdate = lessonsRepository.findById(lessonId).orElseThrow();
        lessonForUpdate.setName(updateDate.getName());
        lessonForUpdate.setSummary(updateDate.getSummary());
        lessonForUpdate.setStartTime(updateDate.getStartTime());
        lessonForUpdate.setFinishTime(updateDate.getFinishTime());
        lessonsRepository.save(lessonForUpdate);
    }
}
