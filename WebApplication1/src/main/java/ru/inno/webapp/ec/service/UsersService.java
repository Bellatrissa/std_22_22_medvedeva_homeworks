package ru.inno.webapp.ec.service;

import ru.inno.webapp.ec.dto.UserForm;
import ru.inno.webapp.ec.model.User;

import java.util.List;

public interface UsersService {
    List<User> getAllUsers();

    void addUser(UserForm user);

    User getUser(Long id);

    void updateUser(Long userId, UserForm user);

    void deleteUser(Long userId);
}
