package ru.inno.webapp.ec.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.webapp.ec.model.Course;
import ru.inno.webapp.ec.model.User;

import java.util.List;

public interface UsersRepository extends JpaRepository<User, Long> {
    List<User> findAllByStateNot(User.State state);

    List<User> findAllByCoursesNotContains(Course course);

    List<User> findAllByCoursesContains(Course section);
}
