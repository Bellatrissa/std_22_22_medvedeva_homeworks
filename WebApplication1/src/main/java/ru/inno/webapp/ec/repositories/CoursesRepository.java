package ru.inno.webapp.ec.repositories;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.webapp.ec.model.Course;

import java.util.List;

public interface CoursesRepository extends JpaRepository<Course, Long> {
    List<Course> findAllByStatusNot(Course.Status status);
}
