public class Main {
    public static void main(String[] args) {
        Figures figures = new Figures(2, 5);
        Square square = new Square(figures.centerX, figures.centerY, 8);
        square.perimeterSquare();
        square.squareArea();
        square.moveSquare(5, 8);
        System.out.println("Периметр квадрата " + square.getPerimeter());
        System.out.println("Площадь квадрата " + square.getArea());
        Rectangle rectangle = new Rectangle(figures.centerX, figures.centerY, square.sideLength, 10);
        rectangle.perimeterRectangle();
        rectangle.areaRectangle();
        rectangle.moveRectangle(6, 9);
        System.out.println("Периметр прямоугольника " + rectangle.getPerimeter());
        System.out.println("Площадь прямоугольника " + rectangle.getArea());
        Circle circle = new Circle(figures.centerX, figures.centerY, 5);
        circle.perimeterCircle();
        circle.areaCircle();
        circle.moveCircle(3, 6);
        System.out.println("Периметр круга " + circle.getPerimeter());
        System.out.println("Площадь круга " + circle.getArea());
        Ellipse ellipse = new Ellipse(figures.centerX, figures.centerY, circle.radius, 10);
        ellipse.perimeterEllipse();
        ellipse.areaEllipse();
        ellipse.moveEllipse(3, 6);
        System.out.println("Периметр эллипса " + ellipse.getPerimeter());
        System.out.println("Площадь эллипса " + ellipse.getArea());
    }
}