public class Square extends Figures{
    protected double sideLength;
    private double perimeter;
    private double area;

    public Square(int x, int y,double sideLength) {
        super(x, y);
        this.sideLength = sideLength;
    }
    public double perimeterSquare () {
        perimeter = sideLength * 4;
        return perimeter;
    }
    public double squareArea () {
        area = sideLength * sideLength;
        return area;
    }
    public void moveSquare (int x, int y) {
        this.centerX = x;
        this.centerY = y;
    }

    public double getPerimeter() {
        return perimeter;
    }
    public double getArea() {
        return area;
    }
}
