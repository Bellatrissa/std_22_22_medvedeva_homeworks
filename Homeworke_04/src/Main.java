import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Кол-во элементов массива: ");
        int length = scanner.nextInt();
        int[] array = new int[length];
        for (int i = 0; i < length; i ++) {
            System.out.print("Значение " + i + " элемента массива: ");
            array[i] = scanner.nextInt();
        }
        int sum1 = Sum(array, 2, 6);
        System.out.println(sum1);
    }
    public static int Sum(int[] a, int from, int to) {
        int sum = 0;
        if (from > to || from < 0 || to >= a.length) {
            return -1;
        }
        for (int i = from; i <= to; i++) {
            sum += a[i];
        }
        return sum;
    }
}