package ru.inno.library.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.library.model.Section;
import ru.inno.library.model.User;

import java.util.List;
import java.util.Optional;

public interface UsersRepository extends JpaRepository<User, Long> {
    List<User> findAllByStateNot(User.State state);

    List<User> findAllBySectionsNotContains(Section section);

    List<User> findAllBySectionsContains(Section section);
    Optional<User> findByEmail(String email);
}
