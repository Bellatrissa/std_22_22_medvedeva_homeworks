package ru.inno.library.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.library.model.Section;

import java.util.List;

public interface SectionsRepository extends JpaRepository<Section, Long> {
    List<Section> findAllByStateNot(Section.State state);
}
