package ru.inno.library.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.inno.library.dto.BookForm;
import ru.inno.library.service.BooksService;

@RequiredArgsConstructor
@RequestMapping(value = "/books")
@Controller
public class BooksController {
    private final BooksService booksService;
    @GetMapping
    public String getBooksPage (Model model){
        model.addAttribute("Books",booksService.getAllBooks());
        return "books_page";
    }
    @GetMapping ("/{book-id}")
    public String getBookPage(@PathVariable("book-id")Long bookId,Model model){
        model.addAttribute("book",booksService.getBook(bookId));
        return "book_page";
    }
    @PostMapping
    public String addBook(BookForm book) {
        booksService.addBook(book);
        return "redirect:/books/";
    }
    @GetMapping("/{book-id}/delete")
    public String updateBook(@PathVariable("book-id") Long bookId) {
        booksService.deleteBook(bookId);
        return "redirect:/books/";
    }
    @PostMapping("/{book-id}/update")
    public String updateBook(@PathVariable("book-id") Long bookId, BookForm book) {
        booksService.updateBook(bookId, book);
        return "redirect:/books/" + bookId;
    }
}
