package ru.inno.library.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.inno.library.dto.SectionForm;
import ru.inno.library.model.Section;
import ru.inno.library.security.details.CustomUserDetails;
import ru.inno.library.service.SectionService;

import java.util.List;

@RequiredArgsConstructor
@RequestMapping(value ="/sections")
@Controller
public class SectionsController {

    private final SectionService sectionService;


    @PostMapping("/{section-id}/readers")
    public String addReaderToSection(@PathVariable("section-id") Long sectionId,
                                     @RequestParam("reader-id") Long readerId) {
        sectionService.addReaderToSection(sectionId, readerId);
        return "redirect:/sections/" + sectionId;
    }
    @PostMapping("/{section-id}/books")
    public String addBookToSection(@PathVariable("section-id") Long sectionId,
                                     @RequestParam("book-id") Long bookId) {
        sectionService.addBookToSection(sectionId, bookId);
        return "redirect:/sections/" + sectionId;
    }

    @GetMapping("/{section-id}")
    public String getSectionPage(@PathVariable("section-id") Long sectionId, Model model) {
        model.addAttribute("section", sectionService.getSection(sectionId));
        model.addAttribute("notInSectionReaders", sectionService.getNotInSectionReaders(sectionId));
        model.addAttribute("inSectionReader", sectionService.getInSectionReaders(sectionId));
        model.addAttribute("notInSectionBooks", sectionService.getNotInSectionBooks());
        model.addAttribute("inSectionBooks", sectionService.getInSectionBooks(sectionId));
        return "/section_page";
    }
    @GetMapping
    public String getSectionsPage(@AuthenticationPrincipal CustomUserDetails customUserDetails, Model model) {
        model.addAttribute("role", customUserDetails.getUser().getRole());
        model.addAttribute("sections",
                List.of(
                        Section.builder().id(1L).name("Фантастика").build(),
                        Section.builder().id(2L).name("Фентези").build(),
                        Section.builder().id(3L).name("Детектив").build()

                ));
        return "/sections_page";
    }

    @PostMapping
    public String addSection(SectionForm section) {
       sectionService.addSection(section);
        return "redirect:/sections/";
    }
    @GetMapping("/{section-id}/delete")
    public String updateSection(@PathVariable("section-id") Long sectionId) {
        sectionService.deleteSection(sectionId);
        return "redirect:/sections/";
    }
    @PostMapping("/{section-id}/update")
    public String updateSection(@PathVariable("section-id") Long sectionId, SectionForm section) {
        sectionService.updateSection(sectionId, section);
        return "redirect:/sections/" + sectionId;
    }

}
