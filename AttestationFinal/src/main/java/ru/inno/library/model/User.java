package ru.inno.library.model;
import javax.persistence.*;
import lombok.*;
import org.hibernate.annotations.Check;

import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(exclude = {"sections"})
@ToString(exclude = {"sections"})
@Builder
@Entity
@Table(name = "reader")


public class User {
    public enum State {
        NOT_CONFIRMED, CONFIRMED, DELETED
    }

    public enum Role {
        USER, ADMIN
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true, nullable = false)
    private String email;

    private String password;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(columnDefinition = "int default 0")
    @Check(constraints = "age >= 0 and age <= 120")
    private Integer age = 0;

    @ManyToMany
    @JoinTable(joinColumns = {@JoinColumn(name = "reader_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "section_id", referencedColumnName = "id")})
    private Set<Section> sections;

    @Enumerated(value = EnumType.STRING)
    private State state;

    @Enumerated(value = EnumType.STRING)
    private Role role;

}


