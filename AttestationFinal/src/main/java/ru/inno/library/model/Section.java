package ru.inno.library.model;
import javax.persistence.*;
import lombok.*;


import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(exclude = {"books", "readers"})
@Entity

public class Section {
    public enum State {
        NOT_CONFIRMED, CONFIRMED, DELETED
    }
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @Column(length = 1000)
    private String description;

    @OneToMany(mappedBy = "section", fetch = FetchType.EAGER)
    private Set<Book> books;

    @ManyToMany(mappedBy = "sections", fetch = FetchType.EAGER)
    private Set<User> readers;
    @Enumerated(value = EnumType.STRING)
    private State state;



}
