package ru.inno.library.model;
import javax.persistence.*;
import lombok.*;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@EqualsAndHashCode(exclude = "section")
@ToString(exclude = "section")


public class Book {
    public enum Status {
        NOT_CONFIRMED, CONFIRMED, DELETED
    }
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;

    @Column(length = 1000)
    private String contents;
    @Column(name="Year_Publishing")
    private String yearPublishing;

    @ManyToOne
    @JoinColumn(name = "section_id")
    private Section section;
    @Enumerated(value = EnumType.STRING)
    private Book.Status status;


}
