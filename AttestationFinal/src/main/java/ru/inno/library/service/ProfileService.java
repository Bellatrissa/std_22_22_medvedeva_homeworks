package ru.inno.library.service;


import ru.inno.library.model.User;
import ru.inno.library.security.details.CustomUserDetails;

public interface ProfileService {
    User getCurrent(CustomUserDetails userDetails);
}
