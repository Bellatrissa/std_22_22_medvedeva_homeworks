package ru.inno.library.service;

import ru.inno.library.dto.SectionForm;
import ru.inno.library.model.Book;
import ru.inno.library.model.Section;
import ru.inno.library.model.User;

import java.util.List;

public interface SectionService {
    List<Section>getAllSections();
    void addReaderToSection(Long sectionId, Long readerId);

    Section getSection(Long sectionId);

    List<User> getNotInSectionReaders(Long sectionId);

    List<User> getInSectionReaders(Long sectionId);

    void deleteSection(Long courseId);

    void addSection(SectionForm section);

    void updateSection(Long sectionId, SectionForm section);
    void addBookToSection(Long sectionId, Long bookId);
    List<Book> getNotInSectionBooks();
    List<Book> getInSectionBooks(Long sectionId);
}
