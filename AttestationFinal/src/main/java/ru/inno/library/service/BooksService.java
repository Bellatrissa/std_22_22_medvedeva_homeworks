package ru.inno.library.service;

import ru.inno.library.dto.BookForm;
import ru.inno.library.model.Book;

import java.util.List;

public interface BooksService {
    List<Book>getAllBooks();
    Book getBook(Long bookId);
    void deleteBook(Long bookId);
    void addBook(BookForm book);
    void updateBook(Long bookId,BookForm book);

}
