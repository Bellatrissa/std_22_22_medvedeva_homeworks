package ru.inno.library.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.library.dto.BookForm;
import ru.inno.library.model.Book;
import ru.inno.library.repositories.BooksRepository;
import ru.inno.library.service.BooksService;

import java.util.List;
@RequiredArgsConstructor
@Service

public class BooksServiceImpl implements BooksService {
    private final BooksRepository booksRepository;

    @Override
    public List<Book> getAllBooks() {
        return booksRepository.findAllByStatusNot(Book.Status.DELETED);
    }

    @Override
    public Book getBook(Long bookId) {
        return booksRepository.findById(bookId).orElseThrow();
    }

    @Override
    public void deleteBook(Long bookId) {
        Book BookForDelete = booksRepository.findById(bookId).orElseThrow();
        BookForDelete.setStatus(Book.Status.DELETED);
        booksRepository.save(BookForDelete);
    }

    @Override
    public void addBook(BookForm book) {
        Book newBook = Book.builder()
                .title(book.getTitle())
                .contents(book.getContents())
                .yearPublishing(book.getYearPublishing())
                .status(Book.Status.NOT_CONFIRMED)
                .build();
        booksRepository.save(newBook);
    }

    @Override
    public void updateBook(Long bookId, BookForm updateDate) {
        Book bookForUpdate = booksRepository.findById(bookId).orElseThrow();
        bookForUpdate.setTitle(updateDate.getTitle());
        bookForUpdate.setContents(updateDate.getContents());
        bookForUpdate.setYearPublishing(updateDate.getYearPublishing());
        booksRepository.save(bookForUpdate);
    }
}
