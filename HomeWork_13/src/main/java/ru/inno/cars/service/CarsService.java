package ru.inno.cars.service;

import lombok.RequiredArgsConstructor;
import ru.inno.cars.models.Car;
import ru.inno.cars.repository.CarsRepository;

@RequiredArgsConstructor
public class CarsService {
    private final CarsRepository carsRepository;
    public void write (Car car){

        carsRepository.save(car);
    }
}
