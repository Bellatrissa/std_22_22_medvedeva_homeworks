public class OddNumbersPrintTask extends AbstractNumbersPrintTask{

    OddNumbersPrintTask(int from, int to) {
        super(from, to);
    }

    @Override
    public void complete() {
        if (to > from) {
            for (int i = from; i < to; i++) {
                if (i % 2 != 0) {
                    System.out.println("Не чётные числа " + i);
                }
            }
        }
        else {
            for (int i = to; i < from; i++) {
                if (i % 2 != 0) {
                    System.out.println("Не чётные числа " + i);
                }
            }
        }
    }
}
