public class Main {
    public static void completeAllTasks(Task[] tasks) {
        for (int i = 0; i < tasks.length; i++) {
            tasks[i].complete();
        }
    }
    public static void main(String[] args) {
        EvenNumbersPrintTask evenNumbersPrintTask = new EvenNumbersPrintTask(1, 10);
        OddNumbersPrintTask oddNumbersPrintTask = new OddNumbersPrintTask(15, 5);
        Task[]tasks = {evenNumbersPrintTask,oddNumbersPrintTask};
        completeAllTasks(tasks);
    }
}
