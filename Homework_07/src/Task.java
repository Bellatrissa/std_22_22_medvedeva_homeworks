public interface Task {
    abstract void complete();
}