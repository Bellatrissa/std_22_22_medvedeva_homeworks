import java.util.List;
public interface CarsRepository {
    List <String> findAllNumberBlackColor();
    String findAllColorMinPrice();
    double findAveragePrice();
    Long countUniqueModel();
}
