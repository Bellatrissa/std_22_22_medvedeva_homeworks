import java.io.*;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CarsRepositoryFileBasedImpl implements CarsRepository {
    private final String fileName;

    public CarsRepositoryFileBasedImpl(String fileName) {

        this.fileName = fileName;
    }

    private static final Function<String, Car> stringToCarMapper = currentCar -> {
        String[] parts = currentCar.split("\\|");
        String numberCar = parts[0];
        String model = parts[1];
        String color = parts[2];
        Integer mileage = Integer.parseInt(parts[3]);
        Integer price = Integer.parseInt(parts[4]);
        return new Car(numberCar, model, color, mileage, price);
    };
    @Override
    public List<String> findAllNumberBlackColor() {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToCarMapper)
                    .filter(car -> car.getColor().equals("Black") || car.getMileage() == (0))
                    .map(Car::getNumberCar)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }

    @Override
    public String findAllColorMinPrice() {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToCarMapper)
                    .min(Comparator.comparingInt(Car::getPrice))
                    .map(Car::getColor)
                    .get();
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }

    public double findAveragePrice() {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToCarMapper)
                    .mapToInt(Car::getPrice)
                    .average()
                    .getAsDouble();
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }

    @Override
    public Long countUniqueModel() {
            try {
                return new BufferedReader(new FileReader(fileName))
                        .lines()
                        .map(stringToCarMapper)
                        .filter(car -> car.getPrice() >= 700)
                        .filter(car -> car.getPrice() < 800)
                        .count();
            } catch (IOException e) {
                throw new UnsuccessfulWorkWithFileException(e);
            }
    }

}

