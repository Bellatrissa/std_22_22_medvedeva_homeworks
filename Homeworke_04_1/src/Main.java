import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Кол-во элементов массива: ");
        int length = scanner.nextInt();
        int[] array = new int[length];
        for (int i = 0; i < length; i++) {
            System.out.print("Значение " + i + " элемента массива: ");
            array[i] = scanner.nextInt();
        }
        even(array);
    }
    public static void even(int[] array) {
        for (int j = 0; j < array.length; j++) {
            if (array[j] % 2 == 0) {
                System.out.println(array[j]);
            }
        }
    }
}